package handler

import (
	"fmt"
	"net/http"
)

/*
 * @Author: your name
 * @Date: 2021-04-05 19:38:28
 * @LastEditTime: 2021-04-05 19:39:33
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /songjintinglinux/go/src/shannon_inf_work/httpserver-readygo/handler/HelloServer.go
 */

//HelloServer ...
func HelloServer(w http.ResponseWriter, req *http.Request) {
	if req.Method == "GET" {
		// // ParseForm解析URL中的查询字符串，并将解析结果更新到r.Form字段
		// r.ParseForm()
		// fmt.Println(r.Form)

		// // 遍历打印解析结果
		// for key, value := range r.Form {
		// 	fmt.Println(key, value)

		// }
		fmt.Println("Inside HelloServer handler")
		fmt.Fprintf(w, "Hello,"+req.URL.Path[1:]+"\n")

		w.Write([]byte("GET:My name is songjinting\n"))
		return
	}
	if req.Method == "POST" {
		w.Write([]byte("POST:My name is songjinting\n"))
		return
	}

}
