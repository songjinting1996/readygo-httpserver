package handler

import (
	"github.com/gin-gonic/gin"
)

// Health 用于检查进程状态，用户可以根据自己的需求定义更复杂的行为检查
func Health(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": "I'm OK",
	})
}
	
