/*
 * @Author: your name
 * @Date: 2021-03-29 19:06:12
 * @LastEditTime: 2021-04-05 21:10:40
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /songjintinglinux/go/src/shannon_inf_work/httpserver-readygo/setup/router.go
 */
package setup

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"

	"git.shannonai.com/inf/http-server/handler"
	"git.shannonai.com/shannon/readygo"
)

// SetupRouter 函数用于添加中间件、将路由与相应的处理函数绑定等功能
func SetupRouter() *gin.Engine {
	// 获得由 readygo 维护的唯一的 *gin.Engine 对象
	r := readygo.Engine()

	// 如果你想使用自定义的中间件，请在此处添加
	// readygo.Use()

	// 注册路由与处理函数绑定，可以使用具名绑定或匿名绑定两种方式
	// 重要！请删除掉不使用的路由与 Handler 函数

	// 重要：进程健康状态检查
	r.GET("/health", handler.Health)

	r.GET("/", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "Ready? Go!",
		})
	})

	//注册回调函数
	http.HandleFunc("/", handler.HelloServer)
	log.Println("start")
	//绑定tcp监听地址，并开始接受请求，然后调用服务端处理程序来处理传入的连接请求
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err.Error())
	}

	// 查看限流策略
	// shylock.Register(r, "/debug")

	return r
}
