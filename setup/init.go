package setup

import (
	"fmt"
	
	"git.shannonai.com/readygo_ecosystem/guard"
	"git.shannonai.com/shannon/readygo"

	"github.com/gin-gonic/gin"

	"go.uber.org/zap"

	"git.shannonai.com/inf/http-server/config"
)

// Init 函数用于项目的初始化
func Init() {
	// 初始化 readygo 基本环境
	readygo.Init()

	// 解析用户级配置文件，配置文件名存放路径必须为 conf/app.yaml 。解析完成
	// 后，即可通过 config.Config 变量来访问应用级配置信息
	config.ParseAppConfig()

	// 用户自定义初始化
	userInit()

	setupSignalCallback()
	setupRecoveryCallback()
	setupGuardDefaultHandleCallback()

	// 绑定路由
	SetupRouter()
}


// setupSignalCallback 设置响应系统信号的回调函数
func setupSignalCallback() {
	// 如果你需要响应系统信号，请在这里注册回调函数
	// readygo.RegisterOnSignal(func(sig os.Signal) {
	// 	readygo.Logger().Infow("User-level callback triggered", "type", sig.String())
	// })
}

// setupRecoveryCallback 用于注册错误恢复中间件的回调函数
func setupRecoveryCallback() {
	bot := readygo.DingBot()
	app := readygo.FrameworkConfig().Name
	version := readygo.FrameworkConfig().Version

	if extra := readygo.FrameworkConfig().Extra; len(extra) > 0 {
		app = app + "-" + extra
	}

	static := "# 应用产生了一次恐慌\n"
	static += fmt.Sprintf("+ 【应　　用】 %s\n", app)
	static += fmt.Sprintf("+ 【版　　本】 %s\n", version)

	readygo.RegisterOnRecovery(func(c *gin.Context, err interface{}) {
		detail := static
		detail += fmt.Sprintf("+ 【请 求 ID】 %s\n", c.MustGet("xid").(string))
		detail += fmt.Sprintf("+ 【访问路径】 %s %s %s\n", c.Request.Method, c.Request.URL.Path, c.Request.URL.RawQuery)
		detail += fmt.Sprintf("+ 【客 户 IP】 %s\n", c.ClientIP())
		detail += fmt.Sprintf("+ 【客 户 UA】 %s\n", c.Request.UserAgent())
		detail += fmt.Sprintf("+ 【错误信息】 \n```\n%+v\n```\n", err)

		title := fmt.Sprintf("%s: %v", app, err)

		bot.Markdown(title, detail).Notify()
	})
}

// setupGuardDefaultHandleCallback 用于设置 guard 库的默认处理回调函数，用户可以
// 根据自己的需求进行修改
func setupGuardDefaultHandleCallback() {
	// 默认处理函数尝试从给定的辅助信息中获得一个 logger，并将错误恢复信息写入
	// 到该 logger 中。
	guard.SetDefaultCallback(func(err, aux interface{}) {
		var logger *zap.SugaredLogger

		if ctx, ok := aux.(*gin.Context); ok {
			logger = readygo.Logger(ctx)
		} else if logr, ok := aux.(*zap.SugaredLogger); ok {
			logger = logr
		}

		if logger != nil {
			stack := fmt.Sprintf("%+v", guard.Stack(4))
			logger.Named("guard").
				Errorw("runtime panic", "err", err, "stack", stack)
		}
	})
}
