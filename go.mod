module git.shannonai.com/inf/http-server

go 1.12

require (
	git.shannonai.com/readygo_ecosystem/guard v1.0.0
	git.shannonai.com/shannon/readygo v0.3.0
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/gin-gonic/gin v1.4.0
	github.com/smartystreets/assertions v1.0.1 // indirect
	github.com/smartystreets/goconvey v1.6.4
	github.com/swaggo/files v0.0.0-20190704085106-630677cd5c14 // indirect
	github.com/swaggo/gin-swagger v1.2.0
	github.com/swaggo/swag v1.7.0
	go.uber.org/zap v1.13.0
)
