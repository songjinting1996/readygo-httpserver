package model

import "git.shannonai.com/shannon/readygo/pkg/problem"

var (
	// ProbUserDefine 是用户定义的错误类型
	ProbUserDefine = problem.Define(400, 0, "about:blank",
		"UserDefinedProblem",
		"problem details")
)
