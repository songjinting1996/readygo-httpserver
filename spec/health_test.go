package spec

import (
	"encoding/json"
	"net/http"
	"testing"

	. "github.com/smartystreets/goconvey/convey"

	"git.shannonai.com/shannon/readygo"
)

// TestHealthRoute 用于测试健康检查路由
func TestHealthRoute(t *testing.T) {
	Convey("当通过 GET 访问 /health 时", t, func() {
		req, err := http.NewRequest("GET", "/health", nil)
		w := readygo.TestRequest(req)

		// 验证返回的状态码
		So(err, ShouldBeNil)

		// 解析JSON
		var response map[string]string
		err = json.Unmarshal([]byte(w.Body.String()), &response)

		// 检查是否存在特定的键
		value, exists := response["message"]

		// 对返回结果做验证
		So(err, ShouldBeNil)
		So(exists, ShouldBeTrue)
		So(value, ShouldEqual, "I'm OK")
	})
}
