package config

import (
	"fmt"
	"os"

	"git.shannonai.com/shannon/readygo"
)

// AppConfig 是由
type AppConfig struct {
  // 在此处添加业务相关设置
}

var (
	Config *AppConfig
)

// ParseAppConfig 解析
func ParseAppConfig() {
	var configs map[string]AppConfig

	if err := readygo.ParseConfig(readygo.AppRoot()+"/conf/app.yaml", &configs); err != nil {
		// 配置文件读取有误
		fmt.Fprintf(os.Stderr, "\x1b[31mreading app config: %s\x1b[0m\n", err)
		os.Exit(1)
	}

	mode := readygo.RunMode()
	if config, find := configs[mode]; find {
		// 检查是否存在零值
		if err := readygo.CheckFieldZeroness(config); err != nil {
			fmt.Fprintf(os.Stderr, "\x1b[31munexpected zero value of application config: %s\x1b[0m\n", err)
			os.Exit(1)
		}

		Config = &config
	} else {
		// 指定了错误的运行环境，需要提示用户有效的运行环境值
		keys := make([]string, 0, len(configs))

		for k := range configs {
			keys = append(keys, k)
		}

		fmt.Fprintf(os.Stderr, "\x1b[31mWrong RUN_MODE, got %s, want one of %v\x1b[0m\n", mode, keys)
		// 错误的运行模式，程序员决定进一步处理
		// stub()
	}
}
